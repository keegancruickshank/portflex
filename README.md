# PortFlex

PortFlex is a local based, OS Agnostic, Electron powered Port Checker.

  - Use your own csv of addresses
  - Add your own addresses

# Windows Installer
https://mega.nz/#!Z4Ay0CiI!DtcUZHNJmAMPQTpnu7L74ombyXq4YqcsPt5oKKca3og

# MacOS Installer
https://mega.nz/#!whR2yKiK!-pMMVvCRUx-jZye4imy0--fklo1ZHTLc4V-waQoWN2g

# New Features!

  - Performance Improvements
  - Priority Bug Fixes

### Building and Editing

For any edits to this repository, please issue a pull request. For bugs or features please tag with correct labels under "Issues".

##### This requires Node.js and NPM

Install the dependencies and devDependencies.
```sh
$ npm install
```

For development environments...
```sh
$ npm run dist
```

For production environments...
```sh
$ npm run pack
```

#### Importing custom CSV file
Access assets directory before build:
```sh
./assets/addresses.csv
```
Replace with your addresses in the same csv format:
SITE | NAME | ADDRESS | PORT

#### Changing refresh interval
Access assets directory before build:
```sh
./assets/options.csv
```
Replace value with your own time in milliseconds

### Future Patch

 - Search Functionality
 - Refresh All
 - Post to Webhook
 - Filter by Site / Grouping
 - Changing options and importing csv within app

License
----

MIT


**Free Software, Hell Yeah!**
